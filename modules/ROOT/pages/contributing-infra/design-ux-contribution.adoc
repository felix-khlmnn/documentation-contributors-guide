= Design & UX contributions

[abstract]
_____
This section provides information for designers, user interface (UI), or user experioence (UX) developer how to contribute to the docs.fedoraproject.org site. 
_____

[NOTE]
====
The following text needs more detailed and adjustment.
====

This page explains how to contribute to the Fedora Docs Team as a designer or user interface (U.I.) / user experience (U.X.) developer.

https://pagure.io/fedora-docs/fedora-docs-ui[*fedora-docs-ui*]::
Sources of a U.I. for the new `docs.fedoraproject.org` site.

https://docs.antora.org/antora-ui-default/build-preview-ui/[*Build a UI Project for Local Preview*]::
Official `antora.org` documentation on how to build a preview site using a custom U.I.
