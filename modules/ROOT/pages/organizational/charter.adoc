= Documentation Team Charter

== Mission

The Fedora Documentation Team coordinates the content and tooling of the documentation at docs.fedoraproject.org with contributions from the broader community.

== Governance

=== Membership

Membership in the Docs team consists of three levels: board member, member, and contributor.
All membership levels require signing the Fedora Project Contributor Agreement.
In addition to the revocation process specified, membership may be removed or modified as a result of Code of Conduct enforcement.

==== Board member

Board membership is granted by a vote of existing board members.
A Fedora contributor requesting board membership must submit a ticket in the team's issue tracker. Existing board members vote within a week of ticket submission.
A simple majority of votes in favor will grant board member status.

Board membership does not expire. Board members are expected to step down if they expect to become inactive.
Board member status may be revoked when a board member becomes inactive or consistently makes technically or socially unacceptable actions in the team.
Removing board member status requires the vote of two-thirds of board team members voting.
At least one-third of all current board members must vote in order for the vote to count.

Board members are in the *docs-admin* group in Fedora Accounts.
This grants *owner*-level permissions in GitLab.

Board members are expected to have a recent history of regular, excellent content and/or tooling contributions and participation in meetings and team discussions.

As peer-recognized leaders of the team, board members will generally be ones to chair meetings and represent the team to other Fedora bodies (e.g. the Fedora Mindshare Committee).

==== Member

Membership is granted by a vote of existing members and board members.
A Fedora contributor requesting membership must submit a ticket in the team's issue tracker.
Existing members and board members vote within a week of ticket submission.
A simple majority of votes in favor will grant member status.

Membership does not expire.
Members are expected to step down if they expect to become inactive.
Member status may be revoked when a member becomes inactive or consistently makes technically or socially unacceptable actions in the team.
Removing member status requires the vote of two-thirds of members and board members voting.

Members are in the *docs* group in Fedora Accounts. This grants *developer*-level permissions in GitLab.

Members are expected to have a recent history of content and/or tooling contributions and participation in meetings and team discussions.

==== Contributor

Contributor status is granted to all Fedora contributors.
A contributor may make content or tooling changes by submitting merge requests or patches.
Contributors are encouraged to participate in team meetings and discussions and to provide feedback and review of other content submissions.

=== Making decisions

The team generally operates by consensus.
Formal votes are not required except when sustained disagreement exists or as otherwise required in this charter.

Except when otherwise specified, a simple majority of voting members and board members are required to change team policy, process, or practice.
Any non-trivial change should be publicly discussed for a minimum of one week before being implemented.

Votes will be open for at least one week in order to allow time for eligible voters to cast their votes.

A minimum of three votes is required.
If fewer than three votes are cast after one week, a single additional vote is sufficient to meet the quorum.

=== Meetings

The team xref:./meetings.adoc[meets regularly] to discuss and coordinate ongoing work.
Meeting chairs are selected on a volunteer basis.

Meetings are primarily for discussion.
Decision-making is left for asynchronous communication in order to be inclusive of members who cannot regularly attend meetings.
However, decisions that have been discussed in accordance with the section above may be finalized in meetings.
