= Git for docs writers
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
v0.0.1, 2022-09-26

Some would-be documentation contributors may not know how to effectively use Git to contribute to Fedora documentation.
This document attempts to provide some guidance.
It is intended to be opinionated to keep from overwhelming the reader.

TIP: This is one way, not the only way.
The instructions in this document are not the only way to accomplish the goals.
In some cases, they may not even be the best way, but they're a way that works.
This is targeted explicitly at beginning git users, and so does not include much in the way of choices or options.
As you get more familiar with git, you may deviate in ways that work best for you.

== Prerequisites

To do anything else on this page, you'll want to have the following:

* A https://fedoraproject.org/wiki/Account_System[Fedora Account System] (FAS) account
* git (`dnf install git`)
* The text editor of your choice
* Basic terminal experience
* Podman if you use a Linux OS such as Fedora (`dnf install podman`) or Docker CE if you use macOS. 


NOTE: While this document was written with Fedora in mind, the steps below work on any operating system with git installed.
See the https://git-scm.com/book/en/v2/Getting-Started-Installing-Git[git documentation] for more information on installing git on other operating systems.

== Before your first edit

When you first get started, you won't have commit access to the git repos for docs.
You'll need to create your own copy, called a "fork".

NOTE: You will need to keep your fork up-to-date with the official repo. 
This is covered later in this document.

TIP: By default, `git clone` will clone a repository into a subdirectory named the same as the repo that exists in the directory you run the command from.
In order to keep your filesystem nice and tidy, you may want to have a dedicated directory that you keep all of your docs repos in.
For example: `$HOME/fedora/docs`

. Go to the upstream Pagure repo
.. Click the '''Clone''' drop-down menu in Pagure
.. Copy the contents of the '''Git''' box
.. From a terminal, go to the directory you want to keep your repository in. For example `cd $HOME/fedora/docs`. You will need to create this directory if it doesn't exist (for example: `mkdir -p $HOME/fedora/docs`)
.. From a terminal, run `git clone -o upstream <GIT URL>`. For example `git clone -o upstream \https://pagure.io/fedora-docs/quick-docs/`. **Note: do not forget `-o upstream`. That's really the essential "trick" of this workflow.**
. Create your forked Pagure repo
.. Go to the Pagure repo you want to fork (for example \https://pagure.io/fedora-docs/quick-docs/). We will refer to this as the ''upstream'' repo from now on.
.. Click the '''Fork''' button at the top.
.. Click the '''Clone''' drop-down menu in Pagure
.. Copy the contents of the '''SSH''' box. If you don't see an '''SSH''' box then that means you don't have an SSH key listed in your Pagure profile. Before you go on to the next step make sure to generate one by following https://docs.pagure.org/pagure/usage/first_steps.html#upload-your-ssh-key[these] instructions. 
. Add your fork to the local checkout
.. From a terminal, go to the directory you cloned above. (For example: `cd $HOME/fedora/docs/quick-docs`)
.. From that directory, run `git remote add origin <SSH URL>`. (For example: `git remote add origin ssh://git@pagure.io/forks/bcotton/fedora-docs/quick-docs.git`)

NOTE: This document uses the terminal for git commands.
However, you may also choose to use a graphical git tool.
The principles of the workflow are the same.

== Before every contribution

Before you start making your contributions, you want to make sure you're up-to-date with the upstream repository.
This means you are making changes to the latest version.

. Ensure you're in your master branch with `git checkout master`
. Get the upstream changes with `git pull`

NOTE: This assumes the repo you're editing uses `master` as the main branch.
Some repositories may have a different workflow.
Check with the team if you're not sure.

TIP: If you use the web view of your forked repo, you will want to push the updated content to your fork.
Do this with `git push origin master`.

== Making a contribution

TIP: It's better to create a new branch even if you don't need it than to wish later that you had created a branch because you're trying to clean up conflicts.
Make branches for even the most trivial contributions and you'll avoid that potential heartbreak.

Now it's time to make the contribution you came here to make.
You'll want to start by creating a new branch to do the work in.

. `git checkout -b ''branch_name''` (where ''branch_name'' is something like `issue8-fix_typos` or `add_git_tips_for_writers`)

TIP: You can name a branch whatever you want.
Descriptive names are good, especially if it's something you'll be working on over several sessions.
If you're working directly on the upstream repo, using a unique and descriptive branch name helps other contributors know what your branch is.

TIP: You can check your current branch with `git branch`.
Your current branch will have a `*` at the beginning of the line.
If you need to switch to an existing branch, use `git checkout ''branch_name''`.

Now you can make whatever changes it is that you want. Fire up your favorite text editor and edit away.
When you're done, it's time to commit the changes.

. `git add ''file(s) that you edited''`
. `git commit`
. Edit the commit message and quit the editor.
  If your system uses vim as the default editor (which it probably does, unless you changed it), type `i` to enter editing mode.
  When you're done writing your commit message, hit the Escape key, type `:wq`, and hit enter.

TIP: Good commit messages are helpful when someone (including future you) looks at the commit history to see what was done.
A good commit message says what the commit does and why.

TIP: Try to keep your commits related to a single logical change.
This makes it easier to undo it if needed.
A "single logical change" may require editing multiple files, which should all be in one commit, but if you're adding unrelated content, do it separately.

Finally, it's time to push your changes from your local machine to the remote server and create the pull request.

. `git push origin ''branch_name''`
. In your web browser, go to your forked repo (for example \https://pagure.io/fork/bcotton/fedora-docs/quick-docs)
. Follow the git forge's instructions for creating a pull request (docs for https://docs.pagure.org/pagure/usage/pull_requests.html[Pagure] and https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request-from-a-fork[GitHub])
